FROM openjdk:17-oracle
MAINTAINER aigul
COPY build/libs/userservice-0.0.1-SNAPSHOT.jar userservice.jar
ENTRYPOINT ["java", "-jar", "userservice.jar"]
